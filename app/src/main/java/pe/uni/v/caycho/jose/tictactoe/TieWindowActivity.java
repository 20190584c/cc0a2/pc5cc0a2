package pe.uni.v.caycho.jose.tictactoe;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;

public class TieWindowActivity extends AppCompatActivity {

    Button btnPlayAgain, btnQuit;

    private String player1, player2;
    private int victories1, victories2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tie_window);
        // Ocultar barra de notificaciones
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Views
        btnPlayAgain = findViewById(R.id.btn_play_again);
        btnQuit = findViewById(R.id.btn_quit);

        // Obtenemos nombre del jugador 1 y 2, y victorias 1 y 2
        Intent intent = getIntent();
        player1 = intent.getStringExtra("PLAYER1");
        player2 = intent.getStringExtra("PLAYER2");
        victories1 = intent.getIntExtra("VICTORIES1", 0);
        victories2 = intent.getIntExtra("VICTORIES2", 0);

        buttonEvents();
    }

    private void buttonEvents(){
        btnPlayAgain.setOnClickListener(v -> {
            Intent intent = new Intent(TieWindowActivity.this, MainActivity.class);
            intent.putExtra("PLAYER1", player1);
            intent.putExtra("PLAYER2", player2);
            intent.putExtra("VICTORIES1", victories1);
            intent.putExtra("VICTORIES2", victories2);
            startActivity(intent);
            finish();
        });

        btnQuit.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(TieWindowActivity.this);
            builder.setMessage(R.string.text_alert_dialog_message)
                    .setTitle(R.string.text_alert_dialog_title)
                    .setCancelable(false)
                    .setPositiveButton(R.string.text_positive, (dialog, which) -> {
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    })
                    .setNegativeButton(R.string.text_negative, (dialog, which) -> dialog.cancel());

            AlertDialog  alert = builder.create();
            alert.show();
        });
    }

}