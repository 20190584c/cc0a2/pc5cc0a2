package pe.uni.v.caycho.jose.tictactoe;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class WinnerWindowActivity extends AppCompatActivity {

    private ImageView imageViewWinner;
    private TextView textViewWinner;
    private Button btnPlayAgain, btnQuit;

    private String winner, player1, player2;
    private int image, victories1, victories2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner_window);
        // Oculta la barra de notificaciones
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Views
        imageViewWinner = findViewById(R.id.image_view_winner);
        textViewWinner = findViewById(R.id.text_view_winner);
        btnPlayAgain = findViewById(R.id.btn_play_again);
        btnQuit = findViewById(R.id.btn_quit);

        // Animaciones
        Animation animation_iv = AnimationUtils.loadAnimation(WinnerWindowActivity.this, R.anim.image_view_animation_rotate);
        Animation animation_tv = AnimationUtils.loadAnimation(WinnerWindowActivity.this, R.anim.text_view_animation_appear);

        // Establecemos animaciones
        imageViewWinner.setAnimation(animation_iv);
        textViewWinner.setAnimation(animation_tv);

        // Obtenemos el ganador, el jugador 1 y 2, y victorias 1 y 2
        Intent intent = getIntent();
        winner = intent.getStringExtra("WINNER");
        image = intent.getIntExtra("WINNER_LOGO", 0);
        player1 = intent.getStringExtra("PLAYER1");
        player2 = intent.getStringExtra("PLAYER2");
        victories1 = intent.getIntExtra("VICTORIES1", 0);
        victories2 = intent.getIntExtra("VICTORIES2", 0);

        // Colocamos texto de felicitaciones al jugador ganador y el simbolo
        textViewWinner.setText(getString(R.string.text_tv_winner, winner));
        imageViewWinner.setImageResource(image);

        // Evento onclick listener para cada botón
        buttonEvents();
    }

    /**
     * Eventos onclicks listener
     */
    private void buttonEvents(){
        btnPlayAgain.setOnClickListener(v -> {
            Intent intent = new Intent(WinnerWindowActivity.this, MainActivity.class);
            intent.putExtra("PLAYER1", player1);
            intent.putExtra("PLAYER2", player2);
            intent.putExtra("VICTORIES1", victories1);
            intent.putExtra("VICTORIES2", victories2);
            startActivity(intent);
            finish();
        });

        btnQuit.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(WinnerWindowActivity.this);
            builder.setMessage(R.string.text_alert_dialog_message)
                    .setTitle(R.string.text_alert_dialog_title)
                    .setCancelable(false)
                    .setPositiveButton(R.string.text_positive, (dialog, which) -> {
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    })
                    .setNegativeButton(R.string.text_negative, (dialog, which) -> dialog.cancel());

            AlertDialog  alert = builder.create();
            alert.show();
        });
    }
}