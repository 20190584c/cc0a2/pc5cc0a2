package pe.uni.v.caycho.jose.tictactoe;

public class Cte {
    // dimension del tablero
    public static final int DIMENSION = 3;

    // matriz de posibles tres en raya
    public static final int[][][] matrixThreeInARow = {
            // filas
            {{0,0}, {0,1}, {0,2}},
            {{1,0}, {1,1}, {1,2}},
            {{2,0}, {2,1}, {2,2}},
            // columnas
            {{0,0}, {1,0}, {2,0}},
            {{0,1}, {1,1}, {2,1}},
            {{0,2}, {1,2}, {2,2}},
            // diagonales
            {{0,0}, {1,1}, {2,2}},
            {{2,0}, {1,1}, {0,2}}
    };

    // Casilla vacia
    public static final int EMPTY_BOX = 0;

    // Jugadores
    public static final int PLAYER1 = 1;
    public static final int PLAYER2 = 2;
}
