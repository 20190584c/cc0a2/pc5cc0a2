package pe.uni.v.caycho.jose.tictactoe;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    ImageView box00, box01, box02,
            box10, box11, box12,
            box20, box21, box22;
    TextView textViewPlayer1, textViewPlayer2, textViewVictories1, textViewVictories2;

    int[][] table;       // tablero
    int dimension,       // dimensión del tablero
            turn,        // turno del jugador
            filledBoxes, // casillas llenas
            victories1,  // número de victorias del jugador 1
            victories2;  // número de victorias del jugador 2
    String player1, player2; // nombre del jugador 1 y jugador 2

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Ocultar barra de notificaciones
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Views
        box00 = findViewById(R.id.box_00);
        box01 = findViewById(R.id.box_01);
        box02 = findViewById(R.id.box_02);
        box10 = findViewById(R.id.box_10);
        box11 = findViewById(R.id.box_11);
        box12 = findViewById(R.id.box_12);
        box20 = findViewById(R.id.box_20);
        box21 = findViewById(R.id.box_21);
        box22 = findViewById(R.id.box_22);
        textViewPlayer1 = findViewById(R.id.text_view_player1);
        textViewPlayer2 = findViewById(R.id.text_view_player2);
        textViewVictories1 = findViewById(R.id.text_view_victories1);
        textViewVictories2 = findViewById(R.id.text_view_victories2);

        // Obtnemos nombres de los judores enviados desde el activity InitialActivity
        Intent intent = getIntent();
        player1 = intent.getStringExtra("PLAYER1");
        player2 = intent.getStringExtra("PLAYER2");

        // Obtnemos número de victorias del los jugadores
        victories1 = getIntent().getIntExtra("VICTORIES1", 0);
        victories2 = getIntent().getIntExtra("VICTORIES2", 0);

        // Colocamos los nombres de los jugadores en la pantalla
        textViewPlayer1.setText(player1);
        textViewPlayer2.setText(player2);

        // Colocamos el número de victorias de los jugadores en pantall
        textViewVictories1.setText(String.valueOf(victories1));
        textViewVictories2.setText(String.valueOf(victories2));

        // Inicializamos los demás atributos
        dimension = Cte.DIMENSION;
        table = new int[dimension][dimension];
        initTable(); // iniciamos tabla con casillas vacias
        turn = Cte.PLAYER1;
        filledBoxes = 0;

        // Evento onclick listener para cada botón
        buttonEvents();
    }

    /**
     * Postcondición: inicializa la tabla con casillas vacias
     */
    private void initTable() {
        int i = 0, j;
        while (i < dimension) {
            j = 0;
            while (j < dimension) {
                table[i][j] = Cte.EMPTY_BOX;
                j++;
            }
            i++;
        }
    }

    /**
     * Eventos onclicks listener
      */
    private void buttonEvents() {
        box00.setOnClickListener(v -> {
            placePiece((ImageView) v, 0, 0);
            checkWinner();
        });

        box01.setOnClickListener(v -> {
            placePiece((ImageView) v, 0, 1);
            checkWinner();
        });

        box02.setOnClickListener(v -> {
            placePiece((ImageView) v, 0, 2);
            checkWinner();
        });

        box10.setOnClickListener(v -> {
            placePiece((ImageView) v, 1, 0);
            checkWinner();
        });

        box11.setOnClickListener(v -> {
            placePiece((ImageView) v, 1, 1);
            checkWinner();
        });

        box12.setOnClickListener(v -> {
            placePiece((ImageView) v, 1, 2);
            checkWinner();
        });

        box20.setOnClickListener(v -> {
            placePiece((ImageView) v, 2, 0);
            checkWinner();
        });

        box21.setOnClickListener(v -> {
            placePiece((ImageView) v, 2, 1);
            checkWinner();
        });

        box22.setOnClickListener(v -> {
            placePiece((ImageView) v, 2, 2);
            checkWinner();
        });
    }

    /**
     * pre condición: 0 <= row < 3 y 0 < column <=3
     * post condición: coloca pieza en la posición row, column del tablero
     */
    private void placePiece(ImageView imageView, int row, int column) {
        if (table[row][column] == 0) { // casilla vacia
            if (turn == 1) { // varificamos turno
                imageView.setImageResource(R.drawable.wolf);
                table[row][column] = 1;
                turn = 2;
            } else {
                imageView.setImageResource(R.drawable.cat);
                table[row][column] = 2;
                turn = 1;
            }
            filledBoxes++;
        }
    }

    /**
     * verifica si hay un ganador o empate
     */
    private void checkWinner() {
        boolean winner1, winner2;

        winner1 = checkThreeInARow(Cte.PLAYER1);
        winner2 = checkThreeInARow(Cte.PLAYER2);

        if (winner1) {
            victories1++;
            Intent intent = new Intent(MainActivity.this, WinnerWindowActivity.class);
            intent.putExtra("WINNER", player1);
            intent.putExtra("PLAYER1", player1);
            intent.putExtra("PLAYER2", player2);
            intent.putExtra("WINNER_LOGO", R.drawable.wolf);
            intent.putExtra("VICTORIES1", victories1);
            intent.putExtra("VICTORIES2", victories2);
            startActivity(intent);
        } else if (winner2) {
            victories2++;
            Intent intent = new Intent(MainActivity.this, WinnerWindowActivity.class);
            intent.putExtra("WINNER", player2);
            intent.putExtra("PLAYER1", player1);
            intent.putExtra("PLAYER2", player2);
            intent.putExtra("WINNER_LOGO", R.drawable.cat);
            intent.putExtra("VICTORIES1", victories1);
            intent.putExtra("VICTORIES2", victories2);
            startActivity(intent);
        } else if (filledBoxes == 9) {
            Intent intent = new Intent(MainActivity.this, TieWindowActivity.class);
            intent.putExtra("PLAYER1", player1);
            intent.putExtra("PLAYER2", player2);
            intent.putExtra("VICTORIES1", victories1);
            intent.putExtra("VICTORIES2", victories2);
            startActivity(intent);
        }

    }

    /**
     * Precondición:
     * el player debe ser 1 o 2
     * Postcondición:
     * retorna true si existe un tres en raya,
     * caso constrario, restorna false
     */
    private boolean checkThreeInARow(int player) {
        int p00, p01, p10, p11, p20, p21;

        for (int[][] matrix : Cte.matrixThreeInARow) {

            p00 = matrix[0][0];
            p01 = matrix[0][1];

            p10 = matrix[1][0];
            p11 = matrix[1][1];

            p20 = matrix[2][0];
            p21 = matrix[2][1];

            if (table[p00][p01] == player && table[p10][p11] == player && table[p20][p21] == player) {
                return true;
            }
        }
        return false;
    }
}
