package pe.uni.v.caycho.jose.tictactoe;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {

    ImageView imageViewSplashLow, imageViewSplashCat;
    TextView textViewSplash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        // Ocultar barra de notificaciones
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Views
        imageViewSplashLow = findViewById(R.id.image_view_splash_low);
        imageViewSplashCat = findViewById(R.id.image_view_splash_cat);
        textViewSplash = findViewById(R.id.text_view_splash);

        // Animaciones
        Animation animation_iv_low = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.translate_animation_up_down);
        Animation animation_iv_cat = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.translate_animation_down_up);
        Animation animation_tv = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.text_view_animation_appear);

        // establecemos animaciones
        imageViewSplashLow.setAnimation(animation_iv_low);
        imageViewSplashCat.setAnimation(animation_iv_cat);
        textViewSplash.setAnimation(animation_tv);

        // Timer
        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {}

            @Override
            public void onFinish() {
                Intent next = new Intent(SplashActivity.this, InitialActivity.class);
                startActivity(next);
                finish();
            }
        }.start();
    }
}