package pe.uni.v.caycho.jose.tictactoe;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

public class InitialActivity extends AppCompatActivity {

    LinearLayout linearLayout;
    EditText editTextPlayer1, editTextPlayer2;
    Button btnPlay;

    SharedPreferences sharedPreferences;
    String player1, player2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);
        // Ocultar barra de notificaciones
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Views
        linearLayout = findViewById(R.id.linear_layout);
        editTextPlayer1 = findViewById(R.id.edit_text_player1);
        editTextPlayer2 = findViewById(R.id.edit_text_player2);
        btnPlay = findViewById(R.id.btn_play);

        // Colocamos nombres de los jugadores en la pantalla
        editTextPlayer1.setText(player1);
        editTextPlayer2.setText(player2);

        buttonEvent();

    }

    private void buttonEvent(){
        btnPlay.setOnClickListener(v->{
            getNamePlayers();
            if (correctData()) {
                Intent intent = new Intent(InitialActivity.this, MainActivity.class);
                intent.putExtra("PLAYER1", player1);
                intent.putExtra("PLAYER2", player2);
                startActivity(intent);
                finish();
            }
        });
    }

    private void getNamePlayers(){
        player1 = editTextPlayer1.getText().toString();
        player2 = editTextPlayer2.getText().toString();
    }


    /**
     * postcondicion:
     * retorna true si los nombres de los jugadores no son vacios,
     * caso contrario retorna false
     */
    private boolean correctData() {
        if (player1.isEmpty() || player2.isEmpty()) {
            Snackbar.make(linearLayout, R.string.text_warning, Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
